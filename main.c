#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define ISIZE 100
#define JSIZE 100

double a[ISIZE][JSIZE], b[ISIZE][JSIZE];

int main(int argc, char **argv)
{
	int i, j;
	FILE *ff;
	if (argc > 1) 
		ff = fopen(argv[1], "w");
	else 
		ff = stdout;

#	pragma omp parallel private(i,j) shared(a,b)
	{
#		pragma omp for 
		for (i=0; i<ISIZE; i++){
			for (j=0; j<JSIZE; j++){
				a[i][j] = 10*i +j;
				b[i][j] = 0.;
			}
		}

#		pragma omp for
		for (i=0; i<ISIZE; i++){
			for (j = 0; j < JSIZE; j++){
				a[i][j] = sin(0.00001*a[i][j]);
			}
		}
#		pragma omp for nowait
		for (i=0; i<ISIZE; i++){
			for (j = 2; j < JSIZE; j++){
				b[i][j] = a[i][j-2]*2.5;
			}
		}
	}
	for(i=0; i < ISIZE; i++){
		for (j=0; j < JSIZE; j++){
			fprintf(ff,"%f ",b[i][j]);
		}
		fprintf(ff,"\n");
	}
	fclose(ff);
}
